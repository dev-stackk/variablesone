package com.algebra.variablesone;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etFirstNumber;
    private EditText etSecondNumber;
    private Button bAdd;
    private Button bSubtract;
    private Button bMultiply;
    private Button bDivide;
    private Button bModulo;
    private Button bTest;
    private TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidgets();
        setupListeners();
    }

    private void setupListeners() {

        bTest.setOnClickListener(view -> {
            byte a;
            a = 5;

            //a = 128;

            byte b = 127;
            //byte c = 5 + b;

            short s = 500;

            //b = s;

            short c = b;

            printResult(c);

            int firstInt = 245;
            firstInt = b;
            firstInt = c;

            int mojInt;
            mojInt = 8;

            short mojShort = 10;

            long l = 34L;

            int i = 6;
            int f = 9;

            int rez = i + f;
            printResult(rez);
            rez = i / f;
            printResult(rez);
            rez = i * f;
            printResult(rez);
            rez = i - f;
            printResult(rez);
            rez = f % i;
            printResult(rez);

            printResult(l);

            int prviBroj = 55;
            int drugiBroj = 77;
            int umnozak = prviBroj * drugiBroj;
            int zbroj = prviBroj + drugiBroj;
            int razlika = prviBroj - drugiBroj;

            printResult(umnozak);
            printResult(zbroj);
            printResult(razlika);

            long incremented = ++l;
            printResult(incremented);

            l = 34;
            printResult(l);
            printResult(l++);
            float fl = 3.2444444444444499993939993f;

            double d = 9.9393993939934444444444444;

            printResult(fl);
            printResult(d);

            double mojDouble;
            mojDouble = 8.95;

            float mojFloat = 26.8f;

            printResult(mojDouble);
            printResult(mojFloat);
        });

        bAdd.setOnClickListener(view -> {
          /*  int firstNumber = getFirstNumber();
            int secondNumber = getSecondNumber();

            int result = firstNumber + secondNumber;

            printResult(result);*/

            double firstNumber = getFirstDecimalNumber();
            double secondNumber = getSecondDecimalNumber();

            double result = firstNumber + secondNumber;

            printResult(result);
        });

        bSubtract.setOnClickListener(view -> {
        /*    int firstNumber = getFirstNumber();
            int secondNumber = getSecondNumber();

            int result = firstNumber - secondNumber;

            printResult(result);*/

            double firstNumber = getFirstDecimalNumber();
            double secondNumber = getSecondDecimalNumber();

            double result = firstNumber - secondNumber;

            printResult(result);
        });

        bMultiply.setOnClickListener(view -> {
           /* int firstNumber = getFirstNumber();
            int secondNumber = getSecondNumber();

            int result = firstNumber * secondNumber;

            printResult(result);*/

            double firstNumber = getFirstDecimalNumber();
            double secondNumber = getSecondDecimalNumber();

            double result = firstNumber * secondNumber;

            printResult(result);
        });

        bDivide.setOnClickListener(view -> {
           /* int firstNumber = getFirstNumber();
            int secondNumber = getSecondNumber();

            int result = firstNumber / secondNumber;

            printResult(result);*/


            double firstNumber = getFirstDecimalNumber();
            double secondNumber = getSecondDecimalNumber();

            double result = firstNumber / secondNumber;

            printResult(result);
        });

        bModulo.setOnClickListener(view -> {
            int firstNumber = getFirstNumber();
            int secondNumber = getSecondNumber();

            int result = firstNumber % secondNumber;

            printResult(result);
        });

    }

    private void printResult(long result) {
        tvResult.append(result + "\n");
        //Toast.makeText(this, "Result: " + result, Toast.LENGTH_LONG).show();
    }

    private void printResult(double result) {
        tvResult.append(result + "\n");
        //Toast.makeText(this, "Result: " + result, Toast.LENGTH_LONG).show();
    }

    private void printResult(float result) {
        tvResult.append(result + "\n");
        //Toast.makeText(this, "Result: " + result, Toast.LENGTH_LONG).show();
    }

    private void initWidgets() {
        etFirstNumber = findViewById(R.id.etNumberOne);
        etSecondNumber = findViewById(R.id.etNumberTwo);
        bAdd = findViewById(R.id.bPlus);
        bSubtract = findViewById(R.id.bMinus);
        bMultiply = findViewById(R.id.bMultiply);
        bDivide = findViewById(R.id.bDivide);
        bModulo = findViewById(R.id.bModulo);
        bTest = findViewById(R.id.bTest);
        tvResult = findViewById(R.id.tvResult);
    }

    private int getFirstNumber() {
        return Integer.parseInt(etFirstNumber.getText().toString());
    }

    private int getSecondNumber() {
        return Integer.parseInt(etSecondNumber.getText().toString());
    }

    private double getFirstDecimalNumber() {
        return Double.parseDouble(etFirstNumber.getText().toString());
    }

    private double getSecondDecimalNumber() {
        return Double.parseDouble(etSecondNumber.getText().toString());
    }
}
